import { Component, OnInit } from '@angular/core';
import { Reservation } from '../model/Reservation';
import { ReservationTime } from '../model/reservationTime';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css']
})
export class ReservationEntryComponent implements OnInit {

  //Properties
  objReservation: Reservation;
  recommendations: String[][]=[["Restaurants", "Rates"],["El celler", "***"],["Diverxo","**"]];
  reservationTimes: ReservationTime[]=[];

  constructor() { }

  ngOnInit() {
    this.createReservationTimes();

    this.objReservation = new Reservation();
  }

  createReservationTimes(): void{
    let reservationTimesAux: string[]=["12:00","13:00","14:00","15:00"];

    for(let i:number=0; i<reservationTimesAux.length;i++){
      this.reservationTimes.push(new ReservationTime
          (i,reservationTimesAux[i]));
    }
  }

  reservationEntry(): void{
   console.log(this.objReservation);
  }
}
