import { Component, OnInit } from '@angular/core';
import {Yeast} from '../model/Yeast';

@Component({
  selector: 'app-levadura',
  templateUrl: './levadura.component.html',
  styleUrls: ['./levadura.component.css']
})
export class LevaduraComponent implements OnInit {

  objYeast:Yeast;

  constructor() { }

  ngOnInit() {
    this.objYeast=new Yeast() ;

  }

  yeastEntry(): void {
    
    console.log(this.objYeast);
    
  }

}
