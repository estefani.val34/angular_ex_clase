import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevaduraComponent } from './levadura.component';

describe('LevaduraComponent', () => {
  let component: LevaduraComponent;
  let fixture: ComponentFixture<LevaduraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevaduraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevaduraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
