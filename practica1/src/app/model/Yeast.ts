
/**
 *
 *
 * @export
 * @class Yeast
 */
export class Yeast { 
    private id : number;
    private class: string;
    private order: string;
    private family: string;
    private genus: string;
    private species:string;
    private reproduction: string;
    private fermentation: string;
    private use: string;
    private pathgenotype:string; //guardare el path de la ruta en string; cuando lo haga cogido lo haré con el servidor la analisis que quiero 
    private pathpicture:string;
    private enterDate:Date;
    private enterTime:string;


	constructor($id: number, $class: string, $order: string, $family: string, $genus: string, $species: string, $reproduction: string, $fermentation: string, $use: string, $pathgenotype: string, $pathpicture: string, $enterDate: Date, $enterTime: string) {
		this.id = $id;
		this.class = $class;
		this.order = $order;
		this.family = $family;
		this.genus = $genus;
		this.species = $species;
		this.reproduction = $reproduction;
		this.fermentation = $fermentation;
		this.use = $use;
		this.pathgenotype = $pathgenotype;
		this.pathpicture = $pathpicture;
		this.enterDate = $enterDate;
		this.enterTime = $enterTime;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $class
     * @return {string}
     */
	public get $class(): string {
		return this.class;
	}

    /**
     * Getter $order
     * @return {string}
     */
	public get $order(): string {
		return this.order;
	}

    /**
     * Getter $family
     * @return {string}
     */
	public get $family(): string {
		return this.family;
	}

    /**
     * Getter $genus
     * @return {string}
     */
	public get $genus(): string {
		return this.genus;
	}

    /**
     * Getter $species
     * @return {string}
     */
	public get $species(): string {
		return this.species;
	}

    /**
     * Getter $reproduction
     * @return {string}
     */
	public get $reproduction(): string {
		return this.reproduction;
	}

    /**
     * Getter $fermentation
     * @return {string}
     */
	public get $fermentation(): string {
		return this.fermentation;
	}

    /**
     * Getter $use
     * @return {string}
     */
	public get $use(): string {
		return this.use;
	}

    /**
     * Getter $pathgenotype
     * @return {string}
     */
	public get $pathgenotype(): string {
		return this.pathgenotype;
	}

    /**
     * Getter $pathpicture
     * @return {string}
     */
	public get $pathpicture(): string {
		return this.pathpicture;
	}

    /**
     * Getter $enterDate
     * @return {Date}
     */
	public get $enterDate(): Date {
		return this.enterDate;
	}

    /**
     * Getter $enterTime
     * @return {string}
     */
	public get $enterTime(): string {
		return this.enterTime;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $class
     * @param {string} value
     */
	public set $class(value: string) {
		this.class = value;
	}

    /**
     * Setter $order
     * @param {string} value
     */
	public set $order(value: string) {
		this.order = value;
	}

    /**
     * Setter $family
     * @param {string} value
     */
	public set $family(value: string) {
		this.family = value;
	}

    /**
     * Setter $genus
     * @param {string} value
     */
	public set $genus(value: string) {
		this.genus = value;
	}

    /**
     * Setter $species
     * @param {string} value
     */
	public set $species(value: string) {
		this.species = value;
	}

    /**
     * Setter $reproduction
     * @param {string} value
     */
	public set $reproduction(value: string) {
		this.reproduction = value;
	}

    /**
     * Setter $fermentation
     * @param {string} value
     */
	public set $fermentation(value: string) {
		this.fermentation = value;
	}

    /**
     * Setter $use
     * @param {string} value
     */
	public set $use(value: string) {
		this.use = value;
	}

    /**
     * Setter $pathgenotype
     * @param {string} value
     */
	public set $pathgenotype(value: string) {
		this.pathgenotype = value;
	}

    /**
     * Setter $pathpicture
     * @param {string} value
     */
	public set $pathpicture(value: string) {
		this.pathpicture = value;
	}

    /**
     * Setter $enterDate
     * @param {Date} value
     */
	public set $enterDate(value: Date) {
		this.enterDate = value;
	}

    /**
     * Setter $enterTime
     * @param {string} value
     */
	public set $enterTime(value: string) {
		this.enterTime = value;
	}


	

}